FROM ubuntu:14.04 as intermediate-download

RUN apt-get update && apt-get install -y ca-certificates subversion

RUN mkdir /build && svn co https://svn.jmodelica.org/tags/2.4 /build/jmodelica

FROM ubuntu:18.04 as intermediate-build

COPY --from=intermediate-download /build/jmodelica /build/jmodelica

RUN apt-get update && apt-get install -y gcc make g++ gfortran python ipython \
                                         swig ant cmake default-jre-headless  \
                                         python-dev python-numpy python-scipy \
                                         python-lxml python-nose python-jpype \
                                         zlib1g-dev libboost-dev cython jcc   \
                                         git patch pkg-config                 \
                                         liblapack-dev libblas-dev wget

ENV LD_LIBRARY_PATH /usr/lib/jvm/default-java/lib/server

COPY build.sh initjcc.patch /

RUN /build.sh

FROM ubuntu:18.04
MAINTAINER alimguzhin@di.uniroma1.it

COPY --from=intermediate-build /opt/jmodelica /opt/jmodelica
COPY --from=intermediate-build /opt/ipopt /opt/ipopt

RUN apt-get update && apt-get install -y gcc make gfortran python ipython     \
                                         default-jre-headless python-numpy    \
                                         python-scipy python-lxml python-nose \
                                         python-jpype zlib1g cython           \
                                         python-matplotlib liblapack3         \
                                         libblas3 \
                                         python-wxgtk3.0

RUN rm -rf /var/lib/apt/lists/*

ENV USER root

RUN mkdir /root/work
WORKDIR /root/work

CMD ["/opt/jmodelica/bin/jm_ipython.sh"]
